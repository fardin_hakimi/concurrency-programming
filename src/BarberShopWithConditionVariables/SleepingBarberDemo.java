package BarberShopWithConditionVariables;

import java.util.Random;

public class SleepingBarberDemo {
	private static Random random= new Random();
	public static void main(String args[]){
		// 10 chairs
		int rand=random.nextInt(15)+5;
		System.out.println("The simulation started with "+rand+" random-generated customers");
		BarberShop shop= new sleepingBarber(10);
		Thread barber= new Thread(new Barber(shop));
		barber.start();
		// random customers
		
		for(int i=1;i<=rand;i++){
		Thread customer= new Thread(new Customer(shop,i));
		customer.start();
		try {
			customer.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
}
