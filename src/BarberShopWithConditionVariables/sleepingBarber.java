package BarberShopWithConditionVariables;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class sleepingBarber implements BarberShop{
    private static int freeChairs;
    private static int chairs;
    private Lock accessLock= new ReentrantLock();
    private Condition customers=accessLock.newCondition();
    private Condition barber= accessLock.newCondition();
    private boolean occupied=false;
    private int customerID;
    Random r= new Random();
    
    sleepingBarber(int c){
    	this.chairs=c;
    	freeChairs=chairs;
    	
    }
	public void HandleCustomer(int id){
		this.customerID=id;
		accessLock.lock();
		if(freeChairs>0){
			System.out.println("customer #"+customerID+" just arrived");
			freeChairs--;
			while(occupied==true){
				System.out.println("customer #"+customerID+" is waiting for his hair cut");
				System.out.println("barber is cutting another customer's hair now");
				try {
					customers.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}// end of while
			getHairCut();
			
			occupied=true;
			barber.signal();
			accessLock.unlock();
	}else{
		System.out.println("Sorry, there is no free seat for you customer #"+customerID+". you have to leave the shop");
		accessLock.unlock();
	}
		
		
	}
	public void getHairCut(){
		System.out.println("Customer #"+customerID+" is receving his haircut now");
		try {
			Thread.sleep(r.nextInt(1000)+500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void cutHair(){
		System.out.println("The barber is cutting hair now");
		System.out.println("next customer please!!!");
		try {
			Thread.sleep(r.nextInt(3500)+2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void HandleBarber() {
		accessLock.lock();
			while(occupied==false){
				System.out.println("The barber is sleeping now");
				System.out.println("Their are no customers in the shop");
				try {
					barber.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}// end of while
			cutHair();
			freeChairs++;
			occupied=false;
			customers.signal();
			accessLock.unlock();
		
	}
}
