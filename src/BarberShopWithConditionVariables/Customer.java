package BarberShopWithConditionVariables;

import java.util.Random;

public class Customer implements Runnable{
     BarberShop shop;
     Random r= new Random();
     int customerId;
     
     Customer(BarberShop s,int id){
    	 shop=s;
    	 this.customerId=id;
    	 
     }
	public void run(){
			try {
				Thread.sleep(r.nextInt(3000)+1000);
				shop.HandleCustomer(customerId);
			} catch (InterruptedException e) {
				e.printStackTrace();
			
		}
		
	}

}
