package BarberShopWithConditionVariables;

public interface BarberShop {
	
	public void HandleCustomer(int id);
	public void HandleBarber();
}
