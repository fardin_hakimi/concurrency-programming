   package sleepingBarber;

   import java.util.concurrent.Semaphore;

   public class BarberAndCustomer implements Runnable{
	
	private static Semaphore customer = new Semaphore(0);
	private static Semaphore barber = new Semaphore(0);
	private static Semaphore seatPermit = new Semaphore(1);

	private static int Chairs;
	private static int numberOfFreeSeats;
	    
	  public BarberAndCustomer(int chairs){
		  Chairs=chairs;
		  numberOfFreeSeats=Chairs;
	  }
	  public void run() {
	    while(true) {  // runs in an infinite loop
	      try {
	       customer.acquire();
	       
	       seatPermit.release();
	       
	       numberOfFreeSeats++; 
	       
	      barber.release();  
	      
	      seatPermit.release(); 
	      this.cutHair();  
	    } catch (InterruptedException ex) {
	    	
	    }
	    }
	  }
	   
	  public void cutHair(){
	    System.out.println("The barber is cutting hair");
	    try {
	      Thread.sleep(5000);
	    } catch (InterruptedException ex){ }
	  }
	  
	  
	  
	  // customer inner class
	  public static class Customer implements Runnable{
		  
	private int customerID;
	private boolean notCut=true;
		  
		    
	public Customer(int i) {
		    customerID = i;
	 }
		 	@Override
	public void run() {
		 		   
	     while (notCut) {
	         try {
	        	 
            seatPermit.acquire();
            
		  if (numberOfFreeSeats > 0) {
		 	    	  
	   System.out.println("Customer #"+ this.customerID + " came and sat down at chair #"+numberOfFreeSeats);
		 	        
	   numberOfFreeSeats--; 
		 	        
	  customer.release();
		 	        
	  seatPermit.release(); 
	try {
	barber.acquire();  
	 notCut = false; 
	 this.getHairCut(); 
    } catch (InterruptedException ex) {
		 	        	
		 	        }
		 	      }   
		 	      else  {
		 	        System.out.println("There are no free seats. Customer #" + this.customerID + " left the barbershop.");
		 	        seatPermit.release(); 
		 	        notCut=false; 
		 	      }
		 	     }
		 	      catch (InterruptedException ex) {
		 	    	  
		 	      }
		 	    }
		 	}
		 	public void getHairCut(){
		 	  System.out.println("Customer " + this.customerID + " is getting a hair cut now");
		 	  System.out.println("Customer " + this.customerID + " got haircut and left the barber shop");
		 	  System.out.println("next customer its your turn!");
		 	  
		 	 if(numberOfFreeSeats==Chairs&&customerID!=1)
		 	   System.out.println(" i am gonna sleep now. no customers");
		 	    try {
		 	    Thread.sleep(500);
		 	    } catch (InterruptedException ex) {}
		 	  }

		 }
}
