package sleepingBarber;

import java.util.Random;

public class BarberShop implements Runnable{
	Random r= new Random();
	int numOfchairs;
	int randomCustomers;
	
	public BarberShop(int chairs){
		this.numOfchairs=chairs;
		
	}
	int getRandomCustomers(){
		return r.nextInt(20)+5;
	}

	@Override
	public void run(){
		   System.out.println(" The multithreaded program started with "+numOfchairs+" chairs");
		   
		   randomCustomers=getRandomCustomers();
		   System.out.println("The number of random generated customers for this Simulation is " +randomCustomers);
		   Thread barber= new Thread(new BarberAndCustomer(numOfchairs));
		   barber.start();
		   
		   for (int i=1; i<=randomCustomers; i++){
			   BarberAndCustomer.Customer c= new BarberAndCustomer.Customer(i);
			   Thread customer= new Thread(c);
		     customer.start();
		     try {
		       Thread.sleep(2000);
		     } catch(InterruptedException ex) {};
		   }
		  } 
		
	}

