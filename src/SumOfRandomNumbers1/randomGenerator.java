package SumOfRandomNumbers1;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class randomGenerator implements Runnable{
	Random randomNumber;
    int random;
    
    public randomGenerator(){
    	randomNumber= new Random();
    }
	public void run() {
		calculate();
	}
	public  synchronized void calculate(){
		try{
			setRandom(randomNumber.nextInt(10)+1);
			TimeUnit.SECONDS.sleep(1);
		}catch(InterruptedException ex){
			System.out.println(Thread.currentThread().getName()+" was interrrupted from sleep");
		}
		System.out.println(Thread.currentThread().getName()+" generated: "+getRandom());
	}
	
	public  void setRandom(int rand){
		this.random=rand;
	}
	public int getRandom(){
		return random;
	}

}
